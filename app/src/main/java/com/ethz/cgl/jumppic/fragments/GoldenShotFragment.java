package com.ethz.cgl.jumppic.fragments;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ethz.cgl.jumppic.R;
import com.ethz.cgl.jumppic.util.Utils;

import java.io.File;


public class GoldenShotFragment extends Fragment {

    private String goldenShotAddress;
    public static GoldenShotFragment newInstance(String address) {
        GoldenShotFragment fragment = new GoldenShotFragment();
        fragment.goldenShotAddress = address;
        return fragment;
    }

    public GoldenShotFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

//        View mainView = inflater.inflate(R.layout.fragment_golden_shot, container, false);

        ImageView pictureView;
        TextView pictureTitle;

        View viewLayout = inflater.inflate(R.layout.full_screen_picture_view, container,
                false);
        pictureView = (ImageView) viewLayout.findViewById(R.id.touchImage_picture_view);
//        pictureView.setImageBitmap(Bitmap.createScaledBitmap(pictures.get(position), viewLayout.getWidth(), viewLayout.getHeight(), false));
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        File pictureFile = new File(goldenShotAddress);
        pictureView.setImageBitmap(Utils.decodeSampledBitmapFromFile(pictureFile, height));
        pictureTitle = (TextView) viewLayout.findViewById(R.id.picture_title);
        pictureTitle.setText(pictureFile.getName().substring(0, pictureFile.getName().length() - 4));
        TextView congratzText = (TextView)viewLayout.findViewById(R.id.congratzView);
        congratzText.setVisibility(View.VISIBLE);
//        container.addView(viewLayout);
        return viewLayout;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
