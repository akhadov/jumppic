package com.ethz.cgl.jumppic.wifip2p;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.ethz.cgl.jumppic.MainActivity;
import com.ethz.cgl.jumppic.MainService;
import com.ethz.cgl.jumppic.util.Utils;

import static com.ethz.cgl.jumppic.util.Utils.log;

/**
 * Receive broadcast from the Android system
 * and send appropriate messages to the main service
 * Created by Sabir on 20.02.2015.
 */
public class WiFip2pBroadcastReceiver extends BroadcastReceiver{

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Handler mainServiceHandler;
    private String TAG = "Wifip2pBroadcastReceiver";

    public WiFip2pBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel,
                                    Handler handler) {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.mainServiceHandler = handler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        log(TAG, "got action " + intent.getAction());
        switch (action) {
            case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                // Check to see if Wi-Fi is enabled and notify appropriate activity
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {

                    // Wifi P2P is enabled
                    log(TAG, "wifi p2p enabled");
                } else {
                    // Wi-Fi P2P is not enabled
                    log(TAG, "wifi p2p NOT enabled");
                    mainServiceHandler.sendEmptyMessage(MainService.HANDLER_MESSAGE_WiFi_NOT_ENABLED);
                }
                break;

            case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                Utils.log(TAG, "Peers changed action");
                if (mManager != null) {
                    mManager.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {

                        @Override
                        public void onPeersAvailable(WifiP2pDeviceList peers) {
                            Message msg = Message.obtain();
                            Bundle data = new Bundle();
                            data.putParcelable("peers", peers);
                            Utils.log(TAG, peers.getDeviceList().size() + "device list size");
                            msg.setData(data);
                            msg.what = MainService.HANDLER_MESSAGE_PEERS_UPDATE;
                            mainServiceHandler.sendMessage(msg);
                        }
                    });
                }
                break;

            case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:

                NetworkInfo networkInfo = (NetworkInfo) intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

                if (networkInfo.isConnected()) {

                    mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {

                        @Override
                        public void onConnectionInfoAvailable(WifiP2pInfo info) {

                            Utils.log(TAG, "Connection info" + info.toString());
                            // if connected inform mainService
                            if (info.groupFormed) {
                                MainService.getInstance().connectHTTP(info);
                            } else {
                                MainService.getInstance().wifiP2PDisconnected();
                            }
                        }
                    });
                }
                else {
                    MainService.getInstance().wifiP2PDisconnected();
                }
                break;

            case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                // Respond to this device's wifi state changing
                WifiP2pDevice d = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
                MainActivity.myMacAddress = d.deviceAddress;
                break;
        }
    }
}
