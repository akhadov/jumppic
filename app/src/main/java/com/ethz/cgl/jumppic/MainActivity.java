package com.ethz.cgl.jumppic;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.NfcManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ethz.cgl.jumppic.camera.CameraActivity;
import com.ethz.cgl.jumppic.fragments.PeerNameFragment;
import com.ethz.cgl.jumppic.fragments.PictureGalleryFragment;
import com.ethz.cgl.jumppic.util.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.ethz.cgl.jumppic.util.Utils.isMyServiceRunning;

/**
 * Android main activity, main service is started here which then runs the program loop
 */
public class MainActivity extends ActionBarActivity implements PeerNameFragment.OnFragmentInteractionListener{

    private List<WifiP2pDevice> peersList;

    private PeersAdapter peersAdapter;

    private PeerNameFragment peerNameFragment;


    private MyReceiver myReceiver;

    private IntentFilter mIntentFilterService;
    private String TAG = "Main Activity";

    private String PEERS_FRAGMENT_TAG = "peersFragmentTag";
    public static String myMacAddress;
    public static String peerMacAddress;

    private LinearLayout pictureView;
    private ArrayList<File> pictures;
    private String PICTURES_FRAGMENT_TAG = "picturesFramentTag";

    public Button jumpBtn;
    private TextView connectingTextView;
    private File pictureStorageDir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Utils.log(TAG, "onCreate");
        setContentView(R.layout.activity_main);
        initializeFields();
        createDebugFile();

        jumpBtn.setEnabled(false);

        subscribeIntents();

        //restore fragment with peers
        if (savedInstanceState != null) {
            peerNameFragment = (PeerNameFragment) getFragmentManager().findFragmentByTag(PEERS_FRAGMENT_TAG);
        }

        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        startService();
    }

    private void subscribeIntents() {

        for (String action : Utils.ACTIONS_MAIN_ACTIVITY){
            mIntentFilterService.addAction(action);
        }
    }

    private void initializeFields() {

        pictures = new ArrayList<>();
        peersList = new ArrayList<>();
        peersAdapter = new PeersAdapter(this, peersList);

        jumpBtn = (Button) findViewById(R.id.jump_btn);
        connectingTextView = (TextView) findViewById(R.id.connecting_text);
        mIntentFilterService = new IntentFilter();

        pictureStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name));
        myReceiver = new MyReceiver();
        pictureView = (LinearLayout) findViewById(R.id.picture_view);
    }

    private void createDebugFile() {

        if (Utils.debug) {
            Context c = getBaseContext();
            if (c != null && c.getExternalFilesDir(null) != null) {
                File log = new File(c.getExternalFilesDir(null).getAbsolutePath() + "/log" + System.currentTimeMillis() + ".txt");
                Utils.setFile(log);
            }
        }
    }

    private void startService(){

        if (!Utils.isMyServiceRunning(MainService.class, (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))) {
            startService(new Intent(this, MainService.class));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.log(TAG, "onDestroy");
        if (Utils.isMyServiceRunning(MainService.class, (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))) {
            stopService(new Intent(MainActivity.this,
                    MainService.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onClick (final View v){

        if (v == findViewById(R.id.jump_btn)) {
            v.setEnabled(false);
            Animation a = AnimationUtils.loadAnimation(getBaseContext(), R.anim.button_click);
            //inform service
            a.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    if (isMyServiceRunning(MainService.class, (ActivityManager) MainActivity.this.getSystemService(Context.ACTIVITY_SERVICE))) {
                        MainService.getInstance().toogleJump();
                    }
                    else {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            Utils.log(TAG, e.getMessage());
                        }
                        startService(new Intent(MainActivity.this, MainService.class));
                    }
                    v.setEnabled(true);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            v.startAnimation(a);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.isAppPaused = true;
        Utils.log(TAG, "onPause");
        unregisterReceiver(myReceiver);
        NfcAdapter adapter = ((NfcManager) getBaseContext().getSystemService(Context.NFC_SERVICE))
                .getDefaultAdapter();
        if (adapter!= null) {
            adapter.disableForegroundDispatch(MainActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableNFC();
        startConnectingAnimation();
        Utils.isAppPaused = false;
        Utils.log(TAG, "onResume");
        loadPictures();

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            Parcelable[] rawMsgs = getIntent().getParcelableArrayExtra(
                    NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage message = (NdefMessage) rawMsgs[0];
            String address = new String(message.getRecords()[0].getPayload());
            Utils.log(TAG, "address is " + address);
            //wait for the service to start
            peerMacAddress = address;
            if (MainService.getInstance() != null)
                MainService.getInstance().setPeerMacAddress(address);
        }
        registerReceiver(myReceiver, mIntentFilterService);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if (getFragmentManager().findFragmentByTag(PICTURES_FRAGMENT_TAG) == null)
            jumpBtn.setVisibility(View.VISIBLE);
    }

    /**
     * load 10 last pictures from pictures folder
     */
    private void loadPictures() {
        if (getFragmentManager().findFragmentByTag(PICTURES_FRAGMENT_TAG) != null)
            return;

        AsyncTask<Void, Void, ArrayList<Bitmap>> t = new AsyncTask<Void, Void, ArrayList<Bitmap>>() {
            @Override
            protected ArrayList<Bitmap> doInBackground(Void... params) {
                pictures.clear();
                File[] picturesF = pictureStorageDir.listFiles();
                if (picturesF == null){
                    return null;
                }
                int length = picturesF.length;
                ArrayList<Bitmap> picturesBitmaps = new ArrayList<>();
                for (int i = length - 1; i >= Math.max(0, length - 10); i--) {
                    File picture = picturesF[i];
                    pictures.add(picturesF[i]);
                    Bitmap pict = Utils.decodeSampledBitmapFromFile(picture, 200);
                    picturesBitmaps.add(pict);
                }
                return picturesBitmaps;
            }

            @Override
            protected void onPostExecute(ArrayList<Bitmap> bitmaps) {
                super.onPostExecute(bitmaps);
                if (bitmaps == null)
                    return;
                MainActivity.this.pictureView.removeAllViews();
                int j = 0;
                for (Bitmap bitmap : bitmaps) {
                    ViewGroup.LayoutParams lparams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    ImageView imageView = new ImageView(MainActivity.this);
                    imageView.setLayoutParams(lparams);
                    imageView.setId(j++);
                    imageView.setPadding(4, 3, 4, 3);
                    imageView.setImageBitmap(bitmap);
                    //set onClick
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = v.getId();
                            getFragmentManager().beginTransaction()
                                    .setCustomAnimations(R.animator.fade_in, R.animator.fade_out, R.animator.fade_in, R.animator.fade_out)
                                    .add(R.id.container, PictureGalleryFragment.newInstance(pictures, position), PICTURES_FRAGMENT_TAG)
                                    .addToBackStack(null).commit();
                        }
                    });
                    MainActivity.this.pictureView.addView(imageView);
                }
                final HorizontalScrollView pictureScrollView = (HorizontalScrollView) findViewById(R.id.picture_scroll_view);
                pictureScrollView.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_MOVE:
                                pictureScrollView.setAlpha(1);
                                break;
                            case MotionEvent.ACTION_UP:
                                Timer timer = new Timer();
                                timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pictureScrollView.setAlpha(0.7f);
                                            }
                                        });
                                    }
                                }, 500);
                                break;
                        }

                        return false;
                    }
                });
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            t.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
        else
            t.execute((Void[])null);
    }

    @Override
    public void onFragmentInteraction(String id) {
        MainService.getInstance().setPeerMacAddress(id);
        if (getFragmentManager().getBackStackEntryCount() != 0)
            getFragmentManager().popBackStack();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Utils.log(TAG, "onNewIntent called");
        setIntent(intent);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void enableNFC() {
        if (Build.VERSION.SDK_INT > 15) {
            NfcManager manager = (NfcManager) getBaseContext().getSystemService(Context.NFC_SERVICE);
            NfcAdapter nfcAdapter = manager.getDefaultAdapter();

            if (nfcAdapter != null && nfcAdapter.isEnabled()) {
                //use NFC to exchange MAC addresses
//                PendingIntent intent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
//                IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
//                try {
//                    ndef.addDataType("application/com.ethz.cgl.jumppic");
//                } catch (IntentFilter.MalformedMimeTypeException e) {
//                    Utils.log(TAG, e.getMessage());
//                }
//                nfcAdapter.enableForegroundDispatch(this, intent, new IntentFilter[]{ndef}, new String[][] { new String[] { MainActivity.class.getName() } });

                nfcAdapter.setNdefPushMessageCallback(new NfcAdapter.CreateNdefMessageCallback() {

                    @Override
                    public NdefMessage createNdefMessage(NfcEvent event) {

                        if (myMacAddress == null) {

                            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                            WifiInfo wInfo = wifiManager.getConnectionInfo();
                            myMacAddress = wInfo.getMacAddress();
                        }
                        NdefMessage message;
                        message = new NdefMessage(new NdefRecord[]{NdefRecord.createMime("application/com.ethz.cgl.jumppic", myMacAddress.getBytes()),
                                NdefRecord.createApplicationRecord("com.ethz.cgl.jumppic")});
                        return message;
                    }
                }, this);
            }
        }
    }


    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            //fight Nexus 7 bug
            jumpBtn.setVisibility(View.VISIBLE);
        }
        else
            super.onBackPressed();
    }




    /**
     * receiver for service messages
     */
    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            Bundle extra = intent.getExtras();
            String action = intent.getAction();

            switch (action){

                case Utils.ACTION_PEERS:
                    Utils.log(TAG, "got peers update");
                    ArrayList<WifiP2pDevice> peers = (ArrayList<WifiP2pDevice>) extra.get("peers");
                    if (peers != null) {
                        //let the user choose
                        if (peers.size() == 0) {
                            if (peerNameFragment != null && peerNameFragment.isAdded()) {
                                getFragmentManager().popBackStack();
                            }
                        } else {
                            if (peerNameFragment != null && peerNameFragment.isAdded()) {
                                Utils.log(TAG, "changing list of peers");
                                peerNameFragment.setNotify(peers);
                            } else {
                                peerNameFragment = new PeerNameFragment();
                                getFragmentManager().beginTransaction()
                                        .setCustomAnimations(R.animator.slide_down, R.animator.slide_up, R.animator.slide_down, R.animator.slide_up)
                                        .add(R.id.container, peerNameFragment, PEERS_FRAGMENT_TAG)
                                        .addToBackStack(null).commit();
                                peerNameFragment.setNotify(peers);
                            }
                        }
                    }
                    break;
                case Utils.ACTION_WiFiP2P_CONNECTED:
                    if (peerNameFragment != null && peerNameFragment.isAdded())
                        getFragmentManager().popBackStack();
                    jumpBtn.setEnabled(true);
                    connectingTextView.setVisibility(View.INVISIBLE);
                    break;
                case Utils.ACTION_WIFIP2P_NOT_AVAILABLE:
                    Toast.makeText(MainActivity.this, "WiFiP2P is not available", Toast.LENGTH_LONG).show();
                    MainActivity.this.finish();
                    System.exit(0);
                    return;
                case Utils.ACTION_NEW_MESSAGE:
                    Utils.log(TAG, "setting message text " + extra.getString(Utils.EXTRA_MESSAGE));
                    Toast.makeText(MainActivity.this, extra.getString(Utils.EXTRA_MESSAGE), Toast.LENGTH_SHORT).show();
                    if (peerNameFragment != null && peerNameFragment.isAdded()){
                        getFragmentManager().popBackStack(PEERS_FRAGMENT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    break;
                case Utils.ACTION_START_CAMERA:
                    getFragmentManager().popBackStack();
                    startActivity(new Intent(MainActivity.this, CameraActivity.class));
                    break;
                case Utils.ACTION_WiFiP2P_DISCONNECTED:
                    jumpBtn.setEnabled(false);
                    connectingTextView.setVisibility(View.VISIBLE);
                    break;
                default:
                    Utils.log(TAG, "unknown action " + action);
            }
        }
    }

    private static int counter = 1;

    private void startConnectingAnimation() {
        if (Utils.connected) {
            connectingTextView.setVisibility(View.INVISIBLE);
            jumpBtn.setEnabled(true);
            return;
        }
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String newText = "Connecting" + Utils.copyChar('.', counter++);
                        connectingTextView.setText(newText);
                        if (counter > 3)
                            counter = 1;
                    }
                });
            }
        }, 0, 1000);
    }
}
