package com.ethz.cgl.jumppic.util;

/**
 * various math functions
 *
 * Created by sabir on 20.05.15.
 */
public class MyMath {

    private static String tag = "MyMath";

    private MyMath(){
        //utility class
    }

    public static float dot(float[] v1, float[] v2) {

        if (v1 == null || v2 == null) {
            Utils.log(tag, "Vector is null");
        }

        if (v1.length < 3 || v2.length < 3) {
            Utils.log(tag, "Vector length < 3");
        }
        float res = 0;
        try {
            res = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static float[] skalarMult(float s, float[] v) {

        if (v == null) {
            Utils.log(tag, "Vector is null");
        }

        if (v.length < 3 ) {
            Utils.log(tag, "Vector length < 3");
        }
        float[] res = new float [3];
        res [0] = v[0]*s;
        res [1] = v[1]*s;
        res [2] = v[2]*s;
        return res;
    }

    public static float norm(float[] v){

        if (v == null) {
            Utils.log(tag, "Vector is null");
        }

        if (v.length < 3 ) {
            Utils.log(tag, "Vector length < 3");
        }
        return (float)Math.sqrt(dot(v,v));
    }

    /**
     *
     * @param v1 first vector
     * @param v2 second vector
     * @return sum v1 + v2
     */
    public static float[] add(float[] v1, float[] v2) {
        if (v1 == null || v2 == null) {
            Utils.log(tag, "Vector is null");
        }

        if (v1.length < 3 || v2.length < 3) {
            Utils.log(tag, "Vector length < 3");
        }

        return new float[]{v1[0] + v2[0],v1[1] + v2[1],v1[2] + v2[2]};
    }

    /**
     * project v1 on v2
     */
    public static float[] project(float[] v1, float[] v2) {
        if (v1 == null || v2 == null) {
            Utils.log(tag, "Vector is null");
        }

        if (v1.length < 3 || v2.length < 3) {
            Utils.log(tag, "Vector length < 3");
        }
        // (v1.v2/(|v2|)^2).v2
        float[] res = MyMath.skalarMult((float)(MyMath.dot(v1, v2) / Math.pow(MyMath.norm(v2), 2)), v2);
        return res;
    }

}
