package com.ethz.cgl.jumppic.util;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.wifi.p2p.WifiP2pDevice;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;

/**
 * Created by Sabir on 22.02.2015.
 */
public final class Utils {

    //constants
    public final static int serverPort = 8888;
    public final static String EXTRA_EST_TIME = "EXTRA_EST_TIME"; //estimated highest point time
    public final static String EXTRA_MESSAGE = "extra message";
    public final static String ACTION_TIME_ESTIMATE = "estimateTime";
    public final static String ACTION_WIFIP2P_NOT_AVAILABLE = "Wifi not available";
    public final static String ACTION_PEERS = "peers update";
    public final static String ACTION_WiFiP2P_CONNECTED = "wifi connected";
    public final static String ACTION_NEW_MESSAGE = "new message";
    //user closed the camera view, stop the jump phase on the peer device
    public final static String ACTION_CAMERA_STOPPED = "camera preview stopped";
    public final static String ACTION_START_CAMERA = "start camera";
    public final static String ACTION_SHOOT = "shoot";
    public final static String ACTION_WiFiP2P_DISCONNECTED = "wifiP2P disconnected";
    public final static String[] ACTIONS_MAIN_ACTIVITY = {ACTION_WiFiP2P_DISCONNECTED, ACTION_NEW_MESSAGE, ACTION_WiFiP2P_CONNECTED, ACTION_PEERS, ACTION_TIME_ESTIMATE, ACTION_WIFIP2P_NOT_AVAILABLE, ACTION_START_CAMERA};
    public final static String ACTION_STOP_CAMERA = "stop camera";
    //stop the peer preview activity
    public final static String ACTION_STOP_JUMP = "stop jump and preview";
    //signal to prepare for the golden shot
    public final static String ACTION_PREPARE = "all systems prepare";

    public static boolean isAppPaused = false;
    public static boolean connected;

    //for how long show the golden shot
    public final static long showPictureFor = 5000;

    //fields
    private static File log;
    public static boolean debug = true;
    private static OutputStreamWriter fileStream;
    private static String TAG = "Utils";

    private Utils (){}

    public static void setFile(File logT){
        log = logT;
        log.delete();
    }

    private static int jumpNumber = 0;

    public static synchronized void log (String tag, String msg) {
        if (debug) {
            if (msg == null)
                return;
//            if (!(msg.contains("important"))) {
//                return;
//            }
            if (msg.contains("accelerating") || msg.contains("got message shoot")){
                log(tag, "\n\n\n" + copyChar((char) jumpNumber++, 30));
            }
            Log.d(tag, msg);
            if (log != null) {
                try {
                    fileStream = new OutputStreamWriter(new FileOutputStream((log), true));
                    String time = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()) + "";
                    fileStream.append(time + " " + tag + " " + msg + "\n");
                } catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                }
                try {
                    fileStream.flush();
                    fileStream.close();
                } catch (IOException e) {
                    Log.e("IoException, Utils.log", e.getMessage());
                }
            } else
                Log.e("Exception", "Log file not provided");
        }
    }

    public static String copyChar(char i, int times) {

        StringBuilder ret =new StringBuilder("");
        for (int j = 0; j < times; j++) {
            ret.append(i + "");
        }
        return ret.toString();
    }
    public static boolean equalsListList(Collection<WifiP2pDevice> L1, Collection<WifiP2pDevice> L2){

        if (L1 == L2)
            return true;
        if (L1 == null || L2 == null)
            return false;
        if (L1.size() != L2.size())
            return false;
        for (Object o : L1){
            if (!o.equals(L2.iterator().next()))
                return false;
        }
        return true;
    }


    //from http://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-in-android
    public static boolean isMyServiceRunning(Class<?> serviceClass, ActivityManager manager) {
//        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static String arrayToString(float fValues[]){
        String res = "";
        for (float v : fValues)
            res += v + " ";
        return res;
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromFile(File picture,
                                                     int reqHeight) {
        if (picture == null)
            return null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picture.getAbsolutePath(), options);

        int reqWidth = (int)(((float)options.outWidth / (float)options.outHeight) * reqHeight);

        //rotate picture
        Matrix m = new Matrix();
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(picture.getAbsolutePath());
        } catch (IOException e) {
            log(TAG, e.getMessage());
        }

        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_ROTATE_90);
        boolean rotate = false;
        switch (orientation){

            case ExifInterface.ORIENTATION_ROTATE_90:
                m.postRotate(90);
                rotate= true;
                reqWidth = reqHeight;
                reqHeight = (int)(((float)options.outHeight / (float)options.outWidth) * reqWidth);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                m.postRotate(180);
                rotate= true;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                m.postRotate(270);
                rotate = true;
                reqWidth = reqHeight;
                reqHeight = (int)(((float)options.outHeight / (float)options.outWidth) * reqWidth);
                break;
        }


        // Calculate inSampleSize
//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        int scaleFactor = (int) (Math.log(options.outHeight / reqHeight)) + 2;
        if (scaleFactor > 1) {
            scaleFactor = (int) Math.pow(2, scaleFactor);
            options.inSampleSize = scaleFactor;
        }
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap ret = BitmapFactory.decodeFile(picture.getAbsolutePath(), options);


        ret = Bitmap.createScaledBitmap(ret, reqWidth, reqHeight, false);
        if (rotate)
            ret = Bitmap.createBitmap(ret, 0, 0, ret.getWidth(), ret.getHeight(), m, true);
//        System.gc();
        return ret;
    }


}
