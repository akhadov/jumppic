package com.ethz.cgl.jumppic;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Sabir on 28.02.2015.
 */
public class PeersAdapter extends BaseAdapter {

    private List<WifiP2pDevice> deviceList;
    private final Context context;
    private String TAG = "PeersAdapter";

    public PeersAdapter (Context context, List<WifiP2pDevice> peers) {
        this.deviceList = peers;
        this.context = context;
    }

    public void setPeers(List<WifiP2pDevice> peers){
        this.deviceList = peers;
    }

    @Override
    public int getCount() {
        return deviceList==null ? 0 : deviceList.size();
    }

    @Override
    public String getItem(int position) {
        return deviceList.get(position).deviceAddress;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final WifiP2pDevice device = deviceList.get(position);
        View row = LayoutInflater.from(context).inflate(R.layout.peer_row, parent, false);
        TextView peerData = (TextView) row.findViewById(R.id.peer_info);
        peerData.setText(device.deviceName);
        return row;
    }
}
