package com.ethz.cgl.jumppic.sensors;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.widget.Toast;

import com.ethz.cgl.jumppic.MainActivity;
import com.ethz.cgl.jumppic.R;
import com.ethz.cgl.jumppic.util.MyMath;
import com.ethz.cgl.jumppic.util.Utils;

/**
 * Created by sabir on 13.05.15.
 *
 * create lin acc and gravity sensors
 * listen to them
 * buffer the last three seconds of the data
 * wait for trigger
 * integrate the data
 * send the message to the main service with the time
 */
public class SensorLogicAcceleration extends Service {

    private NotificationManager mNM;
    private int NOTIFICATION = 865234789;

    private String TAG = "SensorLogicAcceleration";
    private SensorDetector accelerometerSensor;

    private static final double g = 9.8f;

    private SensorManager mSensorManager;
//    private BufferedWriter accFileStream;
//    private BufferedWriter gravityFileStream;
//    private BufferedWriter vertAccFileStream;

    private static final int secondsToBuffer = 15;
    private int minDelay;
    private boolean running;

//    private String gravityFileName;
//    private String accFileName;
//    private String vertAccFileName;
    private long [] accTimestamps;
    private float[][]accValues;
    private float[] verticalAcc;

    private static final float ACC_THRESHOLD = 16f;

    private int size = 1000;

    private PowerManager.WakeLock wakeLock;

    private Thread dataWorkThread;

    private HandlerThread accThread;

    private Handler acclerationHandler;

    private MediaPlayer mAcceleration;
    private MediaPlayer mpRocket;
    private MediaPlayer mBeeps;
    private Intent mIntent;
    private int timeToWaitBeforeNextJump = 3500;
    private int timeSpendAccelerating = 100;

    @Override
    public void onCreate() {
        Utils.log(TAG, "Sensor service created");
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Toast.makeText(this, R.string.jump, Toast.LENGTH_SHORT).show();
        showNotification();
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "SensorWakelockTag");
        //make sure the service is not suspended by the system
        wakeLock.acquire();
        mAcceleration =  MediaPlayer.create(getBaseContext(), R.raw.jump);
        mpRocket =  MediaPlayer.create(getBaseContext(), R.raw.rocket);
        mBeeps = MediaPlayer.create(getBaseContext(), R.raw.beeps);
        mBeeps.setVolume(1, 1);
        mAcceleration.setVolume(1, 1);
        mpRocket.setVolume(1, 1);


        mIntent = new Intent();
        mIntent.setAction(Utils.ACTION_TIME_ESTIMATE);
    }


    @Override
    public void onDestroy() {
        mNM.cancel(NOTIFICATION);
        running = false;
        dataWorkThread = null;
        //unregister listeners from the originating thread
        acclerationHandler.sendEmptyMessage(0);

//        try {
//                accFileStream.flush();
//                gravityFileStream.flush();
//                vertAccFileStream.flush();
//                accFileStream.close();
//                gravityFileStream.close();
//                vertAccFileStream.close();
//        } catch (IOException e) {
//            Utils.log("SensorLogic, onDestroy", e.toString());
//        }

        /* stop threads */

        accThread.interrupt();
        accThread = null;

        mAcceleration.release();
        mpRocket.release();
        wakeLock.release();
    }



    /**
     * Create output files for sensors data
     * register Sensor listeners on two different threads
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        String time = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()) + "";

//        accFileName = "accelerometer" + " on " + time + ".txt";
//        gravityFileName = "gravity" + " on " + time+ ".txt";
//        vertAccFileName = "vert_acc" + " on " + time + ".txt";

//        try {
////            accFileStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((new File(getExternalFilesDir(null), accFileName)), true)));
////            gravityFileStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((new File(getExternalFilesDir(null), gravityFileName)), true)));
////            vertAccFileStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((new File(getExternalFilesDir(null), vertAccFileName)), true)));
////            accFileStream.append("timestamp(ms) values" + "\n");
////            gravityFileStream.append("timestamp(ms) values" + "\n");
////            vertAccFileStream.append("timestamp(ms) value" + "\n");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "restart the app", Toast.LENGTH_SHORT).show();
//            onDestroy();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "restart the app", Toast.LENGTH_SHORT).show();
//            onDestroy();
//        }

        final Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        minDelay = sensor.getMinDelay()/1000; //in milliseconds
        timeSpendAccelerating = sensor.getMaximumRange() > 20 ? 100 : 70;
        Utils.log(TAG, "important sensor max range " + sensor.getMaximumRange());
        size = secondsToBuffer*1000/(minDelay);
        accValues = new float[size][3];
        accTimestamps = new long[size];
        verticalAcc = new float[size];
        accelerometerSensor = new SensorDetector(accValues, accTimestamps);

        accThread = new HandlerThread("acc") {

            @Override
            public void run() {
                Looper.prepare();
                acclerationHandler = new Handler(){

                    @Override
                    public void handleMessage(Message msg){
                        Utils.log(TAG, "unregistering acceleration listener");
                        mSensorManager.unregisterListener(accelerometerSensor);
                    }
                };
                mSensorManager.registerListener(accelerometerSensor,  sensor, 0, acclerationHandler);
                Looper.loop();
            };

        };
        accThread.start();
        workSensorData();
        return START_REDELIVER_INTENT;
    }


    /**
     * Read the buffered values written by sensors thread
     * write them to files
     * compute vertical acceleration
     * wait for trigger
     * integrate
     * pass the estimated time to the mainActivity thread
     */
    private void workSensorData(){

        dataWorkThread = new Thread(){

            @Override
            public void run(){
                try {
                Thread.sleep(1000); //wait to allow the data to be sampled
            } catch (InterruptedException e) {
                Utils.log(TAG, e.toString());
            }
                int i = 0;
                running = true;
                long lastTime = 0;
                int timeout = 0;
                long estTime = Long.MAX_VALUE;

                int state = 0;
                long accTimestamp;
                float[] accValue;
                float verticalAcct;
                long endTime = Long.MAX_VALUE;
                double vel = 0;
                String s;

                mBeeps.start();
                while (running) {
                    if (timeout == 10){
                        mBeeps.start();
                        Utils.log(TAG, "make beeps sound");
                    }
                    i += size;
                    i %= size;

                    accTimestamp = accTimestamps[i];

                    while (accTimestamp < lastTime){
                        Utils.log(TAG, "waiting for sensor");
                        try {
                            Thread.sleep(minDelay * 4); //wait for the sensor to catch up
                        } catch (InterruptedException e) {
                            Utils.log(TAG, e.getMessage());
                        }
                        accTimestamp = accTimestamps[i];
                    }

                    lastTime = accTimestamp;
                    accValue = accValues[i];
                    verticalAcct = MyMath.norm(accValue);
                    verticalAcc[i] = verticalAcct;

                    if (verticalAcct > ACC_THRESHOLD && timeout <= 0) {
                        int counter = timeSpendAccelerating/minDelay;
                        boolean isAccelerating = true;
                        for (int j = i; counter > 0; counter--, j--){
                            j +=size;
                            j %=size;
                            if (verticalAcc[j] < ACC_THRESHOLD - 2){
                                isAccelerating = false;
                                Utils.log(TAG, "FAILFAILFAIL");
                                break;
                            }
                        }
                        if (isAccelerating){
                            Utils.log(TAG, "accelerating");
                            //inform main service
                            sendBroadcast(new Intent().setAction(Utils.ACTION_PREPARE));
                            timeout = timeToWaitBeforeNextJump/minDelay; // wait 3 second before the next toogleJump
                            mAcceleration.start();
                            passTime(200L);
                        }
                    }
                    i++;
                    if (timeout > 0)
                        timeout--;
                    try {
                        Thread.sleep(minDelay-1); //wait for the sensor to catch up
                    } catch (Exception e) {
                        Utils.log(TAG, e.getMessage());
                    }
                }
                stopSelf();
            }
        };
        dataWorkThread.start();
    }

    public class LocalBinder extends Binder {
        SensorLogicAcceleration getService() {
            return SensorLogicAcceleration.this;
        }
    }


    private void passTime(long estTime) {
        mIntent.putExtra(Utils.EXTRA_EST_TIME, estTime);
        sendBroadcast(mIntent);
        Utils.log(TAG, "time broadcast send " + System.currentTimeMillis());
    }

    private final IBinder mBinder = new LocalBinder();

    private void showNotification(){
        String text = "Jump now!";
        String title = "JumpIt";

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);

        Notification.Builder builder = new Notification.Builder(this)
                .setContentIntent(contentIntent)
                .setLargeIcon(icon)
                .setSmallIcon(R.drawable.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setContentText(text)
                .setOngoing(true);

        Notification notification = builder.getNotification();

        // Send the notification
        startForeground(NOTIFICATION, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
