package com.ethz.cgl.jumppic.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

/**
 * receive sensor events from given sensor and save the values to the shared arrays
 */
public class SensorDetector implements SensorEventListener {

    private float[][] dumpValues;
    private long[] dumpTimestamp;
    public int i = 0;
    private int size;
    private String TAG = "SensorDetector";

    public SensorDetector(float [] []dumpValues, long [] dumpT){
        super();
        this.dumpValues = dumpValues;
        size = dumpValues.length;
        dumpTimestamp = dumpT;
    }


    /**
     * write the sensor values to shared arrays
     * @param event received from sensor
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        i %= size;
        dumpValues[i] = event.values.clone();
        long timestamp = event.timestamp;

        dumpTimestamp[i] = timestamp / 1000000; //from nano to ms
        i++;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //DoNothing

    }

}