package com.ethz.cgl.jumppic;

import com.ethz.cgl.jumppic.fragments.GoldenShotFragment;
import com.ethz.cgl.jumppic.util.SystemUiHider;
import com.ethz.cgl.jumppic.util.Utils;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

public class PeerPreviewActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;
    public static final String ACTION_GOLDEN_SHOT = "action golden shot";
    public static final String EXTRA_ROTATION = "preview rotation";
    public static final String EXTRA_GOLDEN_SHOT_ADDRESS = "golden shot extra";
    private static final String TAG_GOLDEN_SHOT = "tag golden shot";

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private ImageView peerPreview;
    private Receiver myReceiver;
    private IntentFilter myIntentFilter;

    public final static String ACTION_NEW_PREVIEW = "new preview";
    public final static String ACTION_STOP_JUMP = "stop jump session";
    public final static String EXTRA_BYTE_ARRAY = "extra byte array";
    private String TAG = PeerPreviewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_preview);

        final View controlsView = findViewById(R.id.stop_button);
        final View contentView = findViewById(R.id.fullscreen_peer_preview);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight() + 250;//60 is the bottom margin for this button
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.stop_button).setOnTouchListener(mDelayHideTouchListener);
        findViewById(R.id.stop_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation a = AnimationUtils.loadAnimation(getBaseContext(), R.anim.button_click);
                a.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        MainService.getInstance().toogleJump();
                        finish();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                v.startAnimation(a);
            }
        });
        peerPreview = (ImageView) findViewById(R.id.fullscreen_peer_preview);
        myReceiver = new Receiver();
        myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(ACTION_NEW_PREVIEW);
        myIntentFilter.addAction(ACTION_STOP_JUMP);
        myIntentFilter.addAction(ACTION_GOLDEN_SHOT);
        final ActionBar actionBar = getActionBar();

        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.isAppPaused = false;
        registerReceiver(myReceiver, myIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.isAppPaused = true;
        unregisterReceiver(myReceiver);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().findFragmentByTag(TAG_GOLDEN_SHOT) != null) {
            getFragmentManager().popBackStack();
            return;
        }
        sendBroadcast(new Intent().setAction(Utils.ACTION_CAMERA_STOPPED));
        MainService.getInstance().toogleJump();
        super.onBackPressed();
    }

    private class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case ACTION_NEW_PREVIEW:
                    Utils.log(TAG, "got broadcast " + action);
                    byte[] preview = intent.getByteArrayExtra(EXTRA_BYTE_ARRAY);
                    Bitmap previewBitmap = BitmapFactory.decodeByteArray(preview, 0, preview.length);
                    Matrix m = new Matrix();
                    int rotation = intent.getIntExtra(EXTRA_ROTATION, 0);
                    m.postRotate(rotation);
                    peerPreview.setImageBitmap(Bitmap.createBitmap(previewBitmap, 0, 0, previewBitmap.getWidth(), previewBitmap.getHeight(), m, false));
                    break;
                case ACTION_STOP_JUMP:
                    finish();
                    break;
                case ACTION_GOLDEN_SHOT:
                    String address = intent.getStringExtra(EXTRA_GOLDEN_SHOT_ADDRESS);
                    if (getFragmentManager().findFragmentByTag(TAG_GOLDEN_SHOT) != null)
                        getFragmentManager().popBackStack();
                    getFragmentManager().beginTransaction()
                            .add(R.id.preview_container, GoldenShotFragment.newInstance(address), TAG_GOLDEN_SHOT)
                            .addToBackStack(null)
                            .commit();
                    Timer t = new Timer();
                    t.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if(getFragmentManager().findFragmentByTag(TAG_GOLDEN_SHOT) != null){
                                getFragmentManager().popBackStack();
                            }
                        }
                    }, Utils.showPictureFor);
                    break;
                default:
                    Utils.log(TAG, "unknown action " + action);
                    break;
            }
        }
    }
}
