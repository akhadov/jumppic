package com.ethz.cgl.jumppic;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;

import com.ethz.cgl.jumppic.camera.CameraActivity;
import com.ethz.cgl.jumppic.camera.CameraLogic;
import com.ethz.cgl.jumppic.sensors.SensorLogic;
import com.ethz.cgl.jumppic.sensors.SensorLogicAcceleration;
import com.ethz.cgl.jumppic.util.Utils;
import com.ethz.cgl.jumppic.wifip2p.ClientLogic;
import com.ethz.cgl.jumppic.wifip2p.ServerLogic;
import com.ethz.cgl.jumppic.wifip2p.WiFiMessage;
import com.ethz.cgl.jumppic.wifip2p.WiFip2pBroadcastReceiver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static com.ethz.cgl.jumppic.util.Utils.log;

/**
 * responsible for communication between devices
 * keeps on running when the screen is turned off
 * no notification is shown
 */
public class MainService extends Service {
    private PowerManager.WakeLock wakeLock;
    private WifiP2pManager wifiP2pManager;
    private android.net.wifi.p2p.WifiP2pManager.Channel wifiChannel;
    private WifiManager wifiManager;
    private WiFip2pBroadcastReceiver wiFip2pBroadcastReceiver;
    private ServerLogic sLogic;


    public final static int HANDLER_MESSAGE_NEW_PREVIEW = 4;
    public final static int HANDLER_MESSAGE_PEERS_UPDATE = 1;
    public final static int HANDLER_MESSAGE_NEW_WIFI_MESSAGE = 2;
    public final static int HANDLER_MESSAGE_SET_PEER_IP = 3;
    public final static int HANDLER_MESSAGE_WiFi_NOT_ENABLED = -1;
    public final static int HANDLER_MESSAGE_GOLDEN_SHOT = 5;


    public final static String PAYLOAD_PREVIEW = "preview";
    public final static String PAYLOAD_NEW_MESSAGE = "new message";
    public final static String PAYLOAD_GOLDEN_SHOT_ADDRESS = "golden shot file address";
    public final static String PAYLOAD_GOLDEN_SHOT = "golden shot";
    public final static String PAYLOAD_IMAGE_TITLE = "payload image title";

    private static Handler myHandler;
    private String TAG = "Main Service";
    public String myMacAdress;
    private String peerMacAdress;

    private ArrayList<WifiP2pDevice> peers;

    //static object of the service
    private static MainService mainService;
    private boolean wifiP2Pconnected;
    private String peerIpAddress;
    private ClientLogic cLogic;
    //peer's clock as of the start of synchronization
    private long peerTime;
    private long minTripTime = Long.MAX_VALUE;
    private long sendTime;
    private long synchronizationStart;
    private boolean stopped;
    private long nanotoMilli = 1000000;
    private long synchronizationStartNanos;

    private long lastSyncedNanos;
    private boolean synchronize;
    private long intentToJumpTime = Long.MAX_VALUE;
    private Receiver myReceiver;

    private boolean synchronizing;
    private int heartbeatIntervalMs = 3000;
    private Thread heartbeatThread;

    public static MainService getInstance(){
        return mainService;
    }

    /**
     * Connecting using wifi p2p:
     * -enable wifi
     * -set own Mac address
     * -discover peers
     *
     */
    @Override
    public void onCreate() {

        Utils.log(TAG, "on create");

        requireWakeLock();
        peers = new ArrayList<>();
        mainService = this;
        initializeHandler();
        initializeWiFi();

        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(Utils.ACTION_TIME_ESTIMATE);
        iFilter.addAction(Utils.ACTION_CAMERA_STOPPED);
        iFilter.addAction(Utils.ACTION_PREPARE);
        myReceiver = new Receiver();
        registerReceiver(myReceiver, iFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.log(TAG, "onDestroy");
        stopped = true;
        wakeLock.release();
        unregisterReceiver(wiFip2pBroadcastReceiver);
        unregisterReceiver(myReceiver);
        //stop server
        if (heartbeatThread != null && heartbeatThread.isAlive()){
            heartbeatThread.interrupt();
            heartbeatThread = null;
        }
        if (sLogic != null)
            sLogic.cleanUp();
        if(cLogic != null)
            cLogic.cleanUp();

        if (Utils.isMyServiceRunning(SensorLogic.class, (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)))
            stopService(new Intent(this,
                    SensorLogic.class));
    }

    /**
     * gets called upon receiving peer address
     * @param address peer MAC address
     */
    public void setPeerMacAddress(String address){
        Utils.log(TAG, "set peer Mac called");
        peerMacAdress = address;
        if (peers != null)
            connectToPeer();
    }

    private void initializeWiFi() {

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);

        if (wifiP2pManager == null || wifiManager == null) {
            Utils.log(TAG, "cannot start wifip2p "+ wifiP2pManager + " wifi " + wifiManager);
            sendBroadcast(getNewIntent(Utils.ACTION_WIFIP2P_NOT_AVAILABLE));
        }
        else {
            wifiChannel = wifiP2pManager.initialize(this, getMainLooper(), null);
            wifiManager.setWifiEnabled(true);
            wiFip2pBroadcastReceiver = new WiFip2pBroadcastReceiver(wifiP2pManager, wifiChannel, myHandler);
            IntentFilter mIntentFilter = new IntentFilter();
            mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
            mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
            mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
            mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
            mIntentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            myMacAdress = wInfo.getMacAddress();
            registerReceiver(wiFip2pBroadcastReceiver, mIntentFilter);
            peerMacAdress = MainActivity.peerMacAddress;
            discoverPeers();
        }
    }

    private void requireWakeLock() {

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "SensorWakelockTag");
        wakeLock.acquire();
    }

    private void initializeHandler() {
        //Handler to communicate with components
        myHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {

                switch (msg.what) {

                    case HANDLER_MESSAGE_WiFi_NOT_ENABLED: {
                        wifiP2PDisconnected();
                        break;
                    }

                    case HANDLER_MESSAGE_GOLDEN_SHOT:
                        //show it on the screen for several seconds
                        sendBroadcast(getNewIntent(CameraActivity.ACTION_GOLDEN_SHOT).putExtra(CameraActivity.EXTRA_GOLDEN_SHOT_ADDRESS, msg.getData().getString(PAYLOAD_GOLDEN_SHOT_ADDRESS)));
                        //send it to peer
                        sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_GOLDEN_SHOT).setPreviewImage(msg.getData().getByteArray(PAYLOAD_GOLDEN_SHOT)).setPreviewRotation(CameraLogic.getCameraRotation()).setTitle(msg.getData().getString(PAYLOAD_IMAGE_TITLE)));
                        break;
                    case HANDLER_MESSAGE_PEERS_UPDATE: {
                        Bundle data = msg.getData();
                        WifiP2pDeviceList deviceList = (WifiP2pDeviceList) data.get("peers");

                        if (deviceList != null && !wifiP2Pconnected) {
                            peers = new ArrayList<>(deviceList.getDeviceList());
                            if (peerMacAdress != null) {
                                connectToPeer();
                            }
                            else {
                                findPeerMacAddress();
                            }

                            for (WifiP2pDevice d : deviceList.getDeviceList())
                                Utils.log(TAG, "devices " + d.deviceAddress);
                        }
                        break;
                    }

                    case HANDLER_MESSAGE_SET_PEER_IP: {
                        Utils.log(TAG, "got ip address");
                        if (peerIpAddress == null) {
                            String ip = msg.getData().getString("peer_ip");
                            setPeerIpAddress(ip);
                        }
                        break;
                    }

                    case HANDLER_MESSAGE_NEW_WIFI_MESSAGE: {
                        WiFiMessage message = (WiFiMessage) msg.getData().getSerializable(PAYLOAD_NEW_MESSAGE);
                        if(message == null)
                            return;
                        Utils.log(TAG, "got message with action " + message.getAction());
                        sendBroadcast(getNewIntent(Utils.ACTION_NEW_MESSAGE).putExtra(Utils.EXTRA_MESSAGE, message.getAction()));
                        switch (message.getAction()){

                            case WiFiMessage.ACTION_PING:
                                sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_PONG).setTime(getCurrentTime()));
                                break;
                            case WiFiMessage.ACTION_PONG:
                                long peerT = message.getTime();
                                long tt = (getCurrentTime() - sendTime) / 2;
                                if (tt < minTripTime) {
                                    minTripTime = tt;
                                    peerTime = peerT + tt + 1;
                                    lastSyncedNanos = System.nanoTime();
                                }
                                sendTime = 0;
                                Utils.log(TAG, "tt is " + tt);
                                break;
                            case WiFiMessage.ACTION_FIST_BUMP:
                                //hello
                                break;
                            case WiFiMessage.ACTION_PREPARE:
                                //stop sending preview frames and fix camera settings
                                sendBroadcast(getNewIntent(CameraActivity.ACTION_PREPARE));
                                break;
                            case WiFiMessage.ACTION_WANT_JUMP:
                                synchronize = false;
                                long time = message.getTime();
                                long localTimeDif = getCurrentTime() - intentToJumpTime;
                                long peerTimeDif = getCurrentPeerTime() - time;
                                Utils.log(TAG, localTimeDif + " peer " + peerTimeDif);
                                if (minTripTime < peerTimeDif - localTimeDif) {
                                    //other device may Jump
                                    //check if the activity is in foreground
                                    if (!Utils.isAppPaused) {
                                        sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_PLEASE_JUMP));
                                        //start camera
                                        sendBroadcast(getNewIntent(Utils.ACTION_START_CAMERA));
                                    }
                                } else {
                                    sendBroadcast(getNewIntent(Utils.ACTION_STOP_CAMERA));
                                }
                                intentToJumpTime = Long.MAX_VALUE;
                                break;
                            case WiFiMessage.ACTION_STOP_CAMERA:
                                sendBroadcast(getNewIntent(Utils.ACTION_STOP_CAMERA));
                                synchronize = true;
                                break;
                            case WiFiMessage.ACTION_STOP_JUMP:
                                toogleJump();
                                break;
                            case WiFiMessage.ACTION_GOLDEN_SHOT:
                                //show it on the screen
                                byte [] image = message.getPreviewImage();
                                String title = message.getTitle();
                                String fileAddress = saveImage(image, title);
                                sendBroadcast(getNewIntent(PeerPreviewActivity.ACTION_GOLDEN_SHOT).putExtra(PeerPreviewActivity.EXTRA_GOLDEN_SHOT_ADDRESS, fileAddress));
                                break;
                            case WiFiMessage.ACTION_PLEASE_JUMP:

                                if(((SensorManager) getSystemService(Context.SENSOR_SERVICE)).getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null){
                                    startService(new Intent(MainService.this, SensorLogic.class));
                                } else{
                                    startService(new Intent(MainService.this, SensorLogicAcceleration.class));
                                }
                                startActivity(new Intent(MainService.this, PeerPreviewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case WiFiMessage.ACTION_SHOOT_AT:
                                long mTime = message.getTime();
                                sendBroadcast(getNewIntent(Utils.ACTION_SHOOT).putExtra(Utils.EXTRA_EST_TIME, mTime - getCurrentTime()));
                                break;
                            case WiFiMessage.ACTION_PREVIEW:
                                //show preview on the screen
                                Utils.log(TAG, "got preview bytes");
                                byte[] preview = message.getPreviewImage();
                                int rotation = message.getPreviewRotation();
                                sendBroadcast(new Intent().setAction(PeerPreviewActivity.ACTION_NEW_PREVIEW).putExtra(PeerPreviewActivity.EXTRA_BYTE_ARRAY, preview).putExtra(PeerPreviewActivity.EXTRA_ROTATION, rotation));
                                break;
                            default:
                                Utils.log(TAG, "unknown action " + message.getAction());
                                break;
                        }
                        break;
                    }

                    case HANDLER_MESSAGE_NEW_PREVIEW:
                        //send preview to peer
                        byte[] preview = msg.getData().getByteArray(PAYLOAD_PREVIEW);
                        sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_PREVIEW).setPreviewImage(preview).setPreviewRotation(CameraLogic.getCameraRotation()));
                        break;
                    default:
                        Utils.log(TAG, "unknown message type " + msg.what);
                        break;

                }
            }
        };
    }

    private String saveImage(byte[] image, String title) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name));
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                log(TAG, "failed to create directory");
                return null;
            }
        }

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +  title);
        try {
            FileOutputStream fos = new FileOutputStream(mediaFile);
            fos.write(image);
            fos.close();
        } catch (IOException e) {
            Utils.log(TAG, e.getMessage());
        }
        return mediaFile.getAbsolutePath();

    }

    //user will choose the peer
    private void findPeerMacAddress() {
        Utils.log(TAG, "finding peer Address");
        sendBroadcast(getNewIntent(Utils.ACTION_PEERS).putExtra("peers", peers));
    }

    private Intent getNewIntent(String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        return intent;
    }

    private void connectToPeer() {
        if (wifiP2Pconnected) {
            Utils.log(TAG, "wifiP2P already connected");
            return;
        }
        for (WifiP2pDevice d : peers){
            if (d.deviceAddress.equals(peerMacAdress)){

                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = peerMacAdress;
                log(TAG, "peer MAC " + config.deviceAddress);
                wifiP2pManager.connect(wifiChannel, config, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        log(TAG, "WiFi p2p connection started");
                    }

                    @Override
                    public void onFailure(int reason) {
                        log(TAG, "WiFi p2p connection fail code: " + reason);
                        if (!wifiManager.isWifiEnabled())
                            wifiManager.setWifiEnabled(true);
                    }
                });

            return;
            }
        }

    }

    private void discoverPeers() {
        log(TAG, "peers discover called");
        if(wifiP2Pconnected || stopped) {
            return;
        }

        wifiP2pManager.discoverPeers(wifiChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                log(TAG, "peers discover started");
            }

            @Override
            public void onFailure(int reasonCode) {
                log(TAG, "peers discover fail code: " + reasonCode);
                if (!wifiManager.isWifiEnabled())
                    wifiManager.setWifiEnabled(true);
            }
        });
    }

    //reconnect if wifiP2P connection was lost
    public void wifiP2PDisconnected() {
        if (!wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(true);
        peers.clear();
        wifiP2Pconnected = false;
        synchronize = false;
        Utils.connected = false;
//        peerIpAddress = null;
        sendBroadcast(getNewIntent(Utils.ACTION_STOP_CAMERA));
        sendBroadcast(getNewIntent(Utils.ACTION_WiFiP2P_DISCONNECTED));
        if (cLogic != null) {
            cLogic.cleanUp();
            cLogic = null;
        }

        if (sLogic != null) {
            sLogic.cleanUp();
            sLogic = null;
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        discoverPeers();
    }

    /**
     * called by WiFip2pBroadcastReceiver
     * at this point the wifi connection is established
     * establish HTTP connection
     * @param info WiFi P2P Info
     */
    public void connectHTTP(WifiP2pInfo info){
        Utils.log(TAG, "start HTTP connection");
        if (!info.groupFormed)
            Utils.log(TAG, "group is not formed, cannot start HTTP connection");
        sLogic = new ServerLogic(myHandler);
        sLogic.startServer();
        wifiP2Pconnected = true;
        Utils.connected = true;
        //inform UI
        sendBroadcast(getNewIntent(Utils.ACTION_WiFiP2P_CONNECTED));

        //HTTP
        //if we are not group owner, we know its address and can send a message
        if (!info.isGroupOwner && info.groupFormed) {
            setPeerIpAddress(info.groupOwnerAddress + "");
        }
    }

    public void sendWiFiMsg(WiFiMessage msg) {

        if (peerIpAddress == null) {
            return;
        }
        Utils.log(TAG, "sending message "  + msg + " " + getCurrentTime() + " peerTime is " + getCurrentPeerTime());
        if (cLogic == null)
            cLogic = new ClientLogic(peerIpAddress);
        cLogic.sendMsg(msg);
    }

    public void setPeerIpAddress(String peerIpAddress) {
        peerIpAddress = peerIpAddress.substring(1);//get rid of the /
        if (peerIpAddress.equals(this.peerIpAddress) && synchronize){
            return;
        }
        this.peerIpAddress = peerIpAddress;
        Utils.log("setPeerIpAddress", "peer ipAddressSet to " + peerIpAddress);
        sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_FIST_BUMP));
        if (heartbeatThread != null && heartbeatThread.isAlive())
            synchronize = true;
        else{
            synchronize = false;
            synchronizeTime();
        }
      }

    /**
     * get the peer time exactly up to rtt
     * also a heartbeat
     */
    private void synchronizeTime (){
        if (synchronizing)
            return;
        heartbeatThread = new Thread (new Runnable() {
            @Override
            public void run() {
                synchronizing = true;
                synchronize = true;
                synchronizationStart = System.currentTimeMillis();
                synchronizationStartNanos = System.nanoTime();
                while (true){
                    while (!synchronize){
                        try {
                            Thread.sleep(heartbeatIntervalMs);
                        } catch (InterruptedException e) {
                            Utils.log(TAG, e.getMessage());
                        }
                    }

                    if (sendTime != 0){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            Utils.log(TAG, e.getMessage());
                        }
                    }
                    sendTime = getCurrentTime();
                    sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_PING).setTime(getCurrentTime()));
                    try {
                        Thread.sleep(heartbeatIntervalMs);
                    } catch (InterruptedException e) {
                        Utils.log(TAG, e.getMessage());
                    }
                }
            }
        });
        heartbeatThread.start();
    }

    //returns time in milliseconds
    //invent the wheel because the internal android clock might
    //be readjusted at any time by the system
    private long getCurrentTime (){
        return synchronizationStart + (System.nanoTime() - synchronizationStartNanos)/ nanotoMilli;
    }

    private long getCurrentPeerTime() {
        return peerTime + (System.nanoTime() - lastSyncedNanos)/ nanotoMilli;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * user pushed Jump button
     * should reach consensus which phone is jumping
     * do nothing if phones not connected yet
     *
     */
    public void toogleJump() {
        Utils.log(TAG, "toggle jump called");

        if (Utils.isMyServiceRunning(SensorLogic.class, (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))) {
            stopService(new Intent(this, SensorLogic.class));
            sendBroadcast(new Intent().setAction(PeerPreviewActivity.ACTION_STOP_JUMP));
            sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_STOP_CAMERA));
            synchronize = true;
            intentToJumpTime = Long.MAX_VALUE;
            Utils.log(TAG, "stopping jump service");
            return;
        }

        if (!wifiP2Pconnected) {
            return;
        }
        synchronize = false;
        intentToJumpTime = getCurrentTime();
        sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_WANT_JUMP).setTime(getCurrentTime()));
    }

    public static Handler getMyHandler() {
        return myHandler;
    }


    //here we receive messages from sensors and relay them over WiFi
    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch(action){
                case Utils.ACTION_TIME_ESTIMATE:
                    long time = intent.getLongExtra(Utils.EXTRA_EST_TIME, -1) - 10;
                    Utils.log(TAG, "got time from sensor " + time);
                    long peerTime = getCurrentPeerTime();
                    sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_SHOOT_AT).setTime(peerTime + time));
                    Utils.log(TAG, "current peer Time " + peerTime);
                    break;
                case Utils.ACTION_CAMERA_STOPPED:
                    sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_STOP_JUMP));
                    break;
                case Utils.ACTION_PREPARE:
                    sendWiFiMsg(new WiFiMessage(WiFiMessage.ACTION_PREPARE));
                    break;
                default:
                    Utils.log(TAG, "unknown action " + action);
                    break;
            }
        }
    }
}