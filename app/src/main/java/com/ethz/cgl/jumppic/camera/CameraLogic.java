package com.ethz.cgl.jumppic.camera;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.view.Surface;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.ethz.cgl.jumppic.MainService;
import com.ethz.cgl.jumppic.R;
import com.ethz.cgl.jumppic.util.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.ethz.cgl.jumppic.util.Utils.log;

/**
 * Everything to do with the camera
 *
 * Created by sabir on 13.03.15.
 */

@SuppressWarnings("deprecation")
public class CameraLogic {

    private static Camera mCamera = null;

    private String TAG = "CameraLogic";
    private CameraPreview mCameraPreview;
    private CameraActivity cameraActivity;

    private Camera.ShutterCallback mShutterCallback;

    private int nPictures = 10;
    private int counter = nPictures + 1;
    private String[] names;
    private long startShooting;
    //time from calling takePicture to shutter close
    private long shootingDelay = 100;
    private long pictureTaken;
    private boolean isTestPicture;
    private Camera.PreviewCallback mPreviewCallback;
    private static int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private static long previewShownAt;
    private final static long SEND_PREVIEW_DELAY = 42; //ms
    private static long previewWidth = 1300;
    private static int cameraRotation;

    public CameraLogic (CameraActivity fragment) {
        cameraActivity = fragment;
        pictures = new byte[nPictures][];
        names = new String[nPictures];
        initializeShutterCallback();
        initializePreviewCallback();
    }

    /**
     * Open the camera
     * set the maximum resolution
     * set orientation
     * start the preview
     */
    public void initialize(){
        Utils.log(TAG, "initialize");
        try {
            mCamera = Camera.open(cameraId);
            parameters = mCamera.getParameters();
            restoreCameraParams();
            List < Camera.Size > sl = parameters.getSupportedPictureSizes();
            int w = 0, h = 0;

            for (Camera.Size s : sl) {
                w = Math.max(s.width, w);
                int ht = s.height;
                if (ht > h && w == s.width)
                    h = ht;

            }

            parameters.setPictureSize(w, h);
            w = Integer.MAX_VALUE; h = Integer.MAX_VALUE;
            for (Camera.Size s :parameters.getSupportedPreviewSizes()) {
                w = Math.min(s.width, w);
                int ht = s.height;
                if (ht < h && w == s.width)
                    h = ht;
                if (w <= previewWidth)
                    break;
                Utils.log(TAG, "width " + s.width + " height " + s.height);
            }

            Utils.log(TAG, "width " + w + " height " + h);
            parameters.setPreviewSize(w, h);
            mCamera.setParameters(parameters);
            Utils.log(TAG, "camera params " + parameters.getPictureSize().width + " " + parameters.getPictureSize().height);
        } catch (Exception e) {
            log(TAG, "Error opening camera " + e.toString());
            Toast.makeText(cameraActivity.getBaseContext(), cameraActivity.getResources().getString(R.string.camera_error), Toast.LENGTH_LONG).show();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e1) {
                Utils.log(TAG, e1.getMessage());
            }
            initialize();
        }

        setCameraDisplayOrientation();
        mCameraPreview = new CameraPreview(cameraActivity.getBaseContext(), mCamera, mPreviewCallback);
        FrameLayout preview = (FrameLayout) cameraActivity.findViewById(R.id.camera_container);
        preview.addView(mCameraPreview);
        isCleaned = false;
    }

    private void initializePreviewCallback() {
        mPreviewCallback = new Camera.PreviewCallback() {
            @Override
            public void onPreviewFrame(byte[] data, Camera camera) {
                if (System.currentTimeMillis() - previewShownAt < SEND_PREVIEW_DELAY)
                    return;
                //send preview to peer
                Camera.Size size = parameters.getPreviewSize();
                Rect rectangle = new Rect();
                rectangle.bottom = size.height;
                rectangle.top = 0;
                rectangle.left = 0;
                rectangle.right = size.width;
                YuvImage image = new YuvImage(data, ImageFormat.NV21,
                        size.width, size.height, null);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                image.compressToJpeg(rectangle, 30, bos); //quality 0-100

                Message m = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putByteArray(MainService.PAYLOAD_PREVIEW, bos.toByteArray());
                m.setData(bundle);
                m.what = MainService.HANDLER_MESSAGE_NEW_PREVIEW;
                MainService.getMyHandler().sendMessage(m);
                previewShownAt = System.currentTimeMillis();
            }
        };
    }

    private void initializeShutterCallback() {
        mShutterCallback = new Camera.ShutterCallback() {
            @Override
            public void onShutter() {
                pictureTaken = System.currentTimeMillis();
                shootingDelay = System.currentTimeMillis() - startShooting;
                cameraActivity.sendBroadcast(new Intent().setAction(Utils.ACTION_NEW_MESSAGE)
                        .putExtra(Utils.EXTRA_MESSAGE, "Shutter Time: " + (shootingDelay)));
            }
        };
    }

    private byte[][] pictures;

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(final byte[] data, Camera camera) {

            mCamera.stopPreview();
            mCamera.startPreview();
            try {
                restoreCameraParams();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (isTestPicture) {
                isTestPicture = false;
                return;
            }
            //show the picture for several seconds on the screen and send it to peer
            File pictureFile = getOutputMediaFile(-1);
            if (pictureFile == null) {
                log(TAG, "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);

                fos.close();
                ExifInterface exif = new ExifInterface(pictureFile.getAbsolutePath());
                android.hardware.Camera.CameraInfo info =
                        new android.hardware.Camera.CameraInfo();
                android.hardware.Camera.getCameraInfo(cameraId, info);
                int rotation = cameraActivity.getResources().getConfiguration().orientation;
                int degrees = 0;
                switch (rotation) {
                    case Configuration.ORIENTATION_PORTRAIT:
                        degrees = ExifInterface.ORIENTATION_ROTATE_90;
                        break;
                }
                exif.setAttribute(ExifInterface.TAG_ORIENTATION, degrees + "");
                exif.saveAttributes();

                byte[] postView = new byte[(int) pictureFile.length()];
                new FileInputStream(pictureFile).read(postView);
                Message m = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putByteArray(MainService.PAYLOAD_GOLDEN_SHOT, postView);
                bundle.putString(MainService.PAYLOAD_GOLDEN_SHOT_ADDRESS, pictureFile.getAbsolutePath());
                bundle.putString(MainService.PAYLOAD_IMAGE_TITLE, pictureFile.getName());
                m.setData(bundle);
                m.what = MainService.HANDLER_MESSAGE_GOLDEN_SHOT;
                MainService.getMyHandler().sendMessage(m);
            } catch (FileNotFoundException e) {
                Utils.log(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Utils.log(TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };

    /** Create a File for saving an image or video */
    private File getOutputMediaFile(int i){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES),  cameraActivity.getResources().getString(R.string.app_name));
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                log(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss").format(new Date());
        File mediaFile;
        if (i < 0){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + timeStamp + ".jpg");
        }else {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "" + names[i] + ".jpg");
        }
        return mediaFile;
    }

    /**
     * add callback which is called
     * when a camera preview frame is available
     */
    private void addPreviewCallback() throws IOException {

        mCamera.reconnect();
        mCamera.setPreviewCallback(mPreviewCallback);
        mCamera.startPreview();
    }

    private void removePreviewCallback() {
        mCamera.setPreviewCallback(null);
    }
    //is the camera closed
    private boolean isCleaned;

    /**
     * Close camera and clean up
     */
    public void cleanUp() {
        if (isCleaned)
            return;
        log(TAG, "cleaning up");
        if (mCamera != null) {
//           if (mCamera.getParameters().getMaxNumDetectedFaces() > 0){
////               mCamera.stopFaceDetection();
//           }
            mCamera.stopPreview();
            mCameraPreview.getHolder().removeCallback(mCameraPreview);
            mCamera.setPreviewCallback(null);
            savePictures();
            mCamera.release();

            FrameLayout preview = (FrameLayout) cameraActivity.findViewById(R.id.camera_container);
            preview.removeView(mCameraPreview);
            isCleaned = true;
            mCamera = null;
        }
    }

    /**
     * Take a picture after i ms
            cLogic.sendWiFiMsg(msg);
     */
    public void shoot(final long i) {
        if (isCleaned)
            return;
        Utils.log(TAG, "shooting method " + System.currentTimeMillis());
        counter = 0;
        Thread t = new Thread (new Runnable() {
            @Override
            public void run() {

                long countdown = i - shootingDelay/2;
                Utils.log(TAG, "shooting in " + countdown);
                if (countdown > 0) {
                    try {
                        Thread.sleep(Math.max(0, countdown));
                    } catch (InterruptedException e) {
                        log(TAG, e.getMessage());
                    }
                }

                Utils.log(TAG, "taking picture " + System.currentTimeMillis());
                startShooting = System.currentTimeMillis();
                try {
                    mCamera.takePicture(mShutterCallback, null, mPicture);
                } catch(RuntimeException takePictureFailed) {
                    Utils.log(TAG, takePictureFailed.getMessage());
                }
            }
        });
        t.start();
    }

    private  Camera.Parameters parameters;

    private void savePictures(){
        int i = 0;
        Camera.Size size = parameters.getPreviewSize();
        Rect rectangle = new Rect();
        rectangle.bottom = size.height;
        rectangle.top = 0;
        rectangle.left = 0;
        rectangle.right = size.width;
        for (int j = 0; j < pictures.length; j++) {
            byte [] data = pictures[j];
            if (data == null) {
                continue;
            }
            YuvImage image = new YuvImage(data, ImageFormat.NV21,
                    size.width, size.height, null);

            File jpgImage = getOutputMediaFile(i);
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(jpgImage);
                image.compressToJpeg(rectangle, 100, fos); //quality 0-100
            } catch (FileNotFoundException e) {
                Utils.log(TAG, e.toString());
            }
            pictures[j] = null;
            i++;
        }
    }

    public void setCameraDisplayOrientation() {

        if (mCamera == null || cameraId == -1) {
            return;

        }
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = cameraActivity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        Camera.Parameters p = mCamera.getParameters();
        p.setRotation(result);
        cameraRotation = result;
        mCamera.setParameters(p);
        mCamera.setDisplayOrientation(result);
    }

    public static int getCameraRotation() {
        return cameraRotation;
    }

    public void prepareToShoot() {
        //fix camera params
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
//        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
//        parameters.setExposureCompensation(parameters.getMinExposureCompensation());
//        if (mCamera.getParameters().isAutoExposureLockSupported()) {
//            parameters.setAutoExposureLock(true);
//        }
//        if (mCamera.getParameters().isAutoWhiteBalanceLockSupported()) {
//            parameters.setAutoWhiteBalanceLock(true);
//        }
//        mCamera.setParameters(parameters);
//        removePreviewCallback();
    }

    private void restoreCameraParams() throws IOException {
        parameters = mCamera.getParameters();
        if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        else if (parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO))
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        if (mCamera.getParameters().getSupportedSceneModes().contains(Camera.Parameters.SCENE_MODE_ACTION)) {
            parameters.setSceneMode(Camera.Parameters.SCENE_MODE_ACTION);
        } else if (mCamera.getParameters().getSupportedSceneModes().contains(Camera.Parameters.SCENE_MODE_SPORTS)) {
            parameters.setSceneMode(Camera.Parameters.SCENE_MODE_SPORTS);
        }
        mCamera.setParameters(parameters);
        addPreviewCallback();
    }
}
