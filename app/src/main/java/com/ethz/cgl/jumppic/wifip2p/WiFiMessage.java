package com.ethz.cgl.jumppic.wifip2p;

import java.io.Serializable;

/**
 * serializable message to exchange information between devices
 * over http
 * Created by sabir on 15.07.15.
 */
public class WiFiMessage implements Serializable{

    public final static String ACTION_PING = "ping";
    public final static String ACTION_PONG = "pong";
    public final static String ACTION_PREVIEW = "preview picture";
    public final static String ACTION_FIST_BUMP = "fist bump";
    //stop jumping session
    public final static String ACTION_STOP_CAMERA = "stop jump";
    //signal that the user wants to toogleJump
    public final static String ACTION_WANT_JUMP = "want to jump";
    //shoot a picture at time
    public final static String ACTION_SHOOT_AT = "shoot at";
    //signal the other device, it can start jumping
    public final static String ACTION_PLEASE_JUMP = "please jump";
    //signal the other device to stop jump session
    public final static String ACTION_STOP_JUMP = "stop sensors";
    //signal the other device to stop sending preview frames and fix all camera settings
    public final static String ACTION_PREPARE = "prepare all systems";
    //signal the other device a jump picture is in payload
    public static final String ACTION_GOLDEN_SHOT = "golden shot";
    private String action;
    private String title;
    private byte[] previewImage;
    private long time = -1;
    private int previewRotation;

    public WiFiMessage(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public byte[] getPreviewImage() {
        return previewImage;
    }

    public WiFiMessage setPreviewImage(byte[] previewImage) {
        this.previewImage = previewImage;
        return this;
    }

    public long getTime() {
        return time;
    }

    public WiFiMessage setTime(long time) {
        this.time = time;
        return this;
    }

    public int getPreviewRotation() {
        return previewRotation;
    }

    public WiFiMessage setPreviewRotation(int previewRotation) {
        this.previewRotation = previewRotation;
        return this;
    }

    public WiFiMessage setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getTitle(){
        return title;
    }
}