package com.ethz.cgl.jumppic.camera;

import com.ethz.cgl.jumppic.fragments.GoldenShotFragment;
import com.ethz.cgl.jumppic.R;
import com.ethz.cgl.jumppic.util.Utils;
import com.ethz.cgl.jumppic.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class CameraActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    private static CameraLogic cameraLogic;
    private IntentFilter mIntentFilter;
    private Receiver mReceiver;
    private String TAG = CameraActivity.class.getSimpleName();
    public final static String ACTION_PREPARE = "prepare all systems";
    public final static String EXTRA_GOLDEN_SHOT_ADDRESS = "golden shot address";
    public final static String ACTION_GOLDEN_SHOT = "golden shot";
    private String TAG_GOLDEN_SHOT = "golden shot tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);

        final View contentView = findViewById(R.id.camera_container);

        cameraLogic = new CameraLogic(this);
        mReceiver = new Receiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Utils.ACTION_SHOOT);
        mIntentFilter.addAction(Utils.ACTION_STOP_CAMERA);
        mIntentFilter.addAction(Utils.ACTION_NEW_MESSAGE);
        mIntentFilter.addAction(ACTION_PREPARE);
        mIntentFilter.addAction(ACTION_GOLDEN_SHOT);
//         Set up an instance of SystemUiHider to control the system UI for
//         this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {


                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });


        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        Settings.System.putInt(getBaseContext().getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 1);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().findFragmentByTag(TAG_GOLDEN_SHOT) != null) {
            getFragmentManager().popBackStack();
            return;
        }
        sendBroadcast(new Intent().setAction(Utils.ACTION_CAMERA_STOPPED));
        super.onBackPressed();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.isAppPaused = false;
        registerReceiver(mReceiver, mIntentFilter);
        cameraLogic.initialize();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.isAppPaused = true;
        unregisterReceiver(mReceiver);
        cameraLogic.cleanUp();
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        cameraLogic.setCameraDisplayOrientation();
    }

    class Receiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {

                case Utils.ACTION_STOP_CAMERA:
                    finish();
                    break;
                case Utils.ACTION_SHOOT:
                    cameraLogic.shoot(intent.getLongExtra(Utils.EXTRA_EST_TIME, -1));
                    Utils.log(TAG, "estimated time " + intent.getLongExtra(Utils.EXTRA_EST_TIME, -1)) ;
                    break;
                case Utils.ACTION_NEW_MESSAGE:
                    String message = intent.getStringExtra(Utils.EXTRA_MESSAGE);
                    if (message.contains("Shutter Time:")) {
                        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case ACTION_PREPARE:
                    cameraLogic.prepareToShoot();
                    break;
                case ACTION_GOLDEN_SHOT:
                    //show it on the screen
                    getFragmentManager().beginTransaction()
                            .add(R.id.camera_container, GoldenShotFragment.newInstance(intent.getStringExtra(EXTRA_GOLDEN_SHOT_ADDRESS)), TAG_GOLDEN_SHOT)
                            .addToBackStack(null)
                            .commit();

                    Timer t = new Timer();
                    t.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if (getFragmentManager().findFragmentByTag(TAG_GOLDEN_SHOT) != null) {
                                getFragmentManager().popBackStack();
                            }
                        }
                    }, Utils.showPictureFor);
                    break;
                default:
                    Utils.log(TAG, "unknown action");
                    break;
            }
        }
    }
}
