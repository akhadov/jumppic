package com.ethz.cgl.jumppic.wifip2p;

import com.ethz.cgl.jumppic.util.Utils;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayDeque;
import java.util.Queue;

import static com.ethz.cgl.jumppic.util.Utils.log;

/**
 * Send messages over http to the server on the peer device
 *
 * Created by Sabir on 28.02.2015.
 */
public class ClientLogic {

    private int port = Utils.serverPort;
    private String hostIp;
    private Socket socket;
    private OutputStream outputStream;
    private boolean stopped;
    private static String TAG = "Client Logic";
    private Queue<WiFiMessage> messages;
    private ObjectOutputStream objectOutputStream;


    public ClientLogic (String hostIp) {
        stopped = false;
        messages = new ArrayDeque<>();
        this.hostIp = hostIp;
    }

    /**
     * Sends asynchronously a message to the server.
     * Manages the socket connection.
     * @param msg string message to send
     */
    public void sendMsg (final WiFiMessage msg) {
        if (!messages.contains(msg)) {
            messages.add(msg); //add at the end of this queue
        }

        if (stopped) {
            return;
        }

        log(TAG, "sending " + msg.getAction() + " " + System.currentTimeMillis());

        if (socket == null || !socket.isConnected() || outputStream == null || objectOutputStream == null) {
            connectSocket();
        }
        try {
            if (objectOutputStream != null) {
                objectOutputStream.writeObject(msg);
                outputStream.flush();
                messages.remove(msg);
                if (messages.peek() != null) {
                    sendMsg(messages.peek());
                }
            }
        }

        catch (IOException e) {
            Utils.log(TAG, e.getMessage());
            if (!stopped) {
                try {
                    Thread.sleep(100);
                }
                catch (InterruptedException e1) {
                    Utils.log(TAG, e1.getMessage());
                }
                connectSocket();
            }
        }
    }

    private void connectSocket(){
        if (stopped) {
            return;
        }
        try {
            Thread t = new Thread (new Runnable() {
                @Override
                public void run() {
                    socket = new Socket();
                    try {
                        socket.setKeepAlive(true); //we have to maintain the connection, until the app is closed
                    } catch (IOException e) {
                       Utils.log(TAG, e.getMessage());
                    }
                    try {
                        socket.connect((new InetSocketAddress(hostIp, port)), 0);
                    } catch (IOException e) {
                        Utils.log(TAG, e.toString());
                    }
                }
            });
            t.start();
            t.join();
            outputStream = socket.getOutputStream();
            objectOutputStream = new ObjectOutputStream(outputStream);
            Utils.log(TAG, "socket connected");

            } catch (IOException | InterruptedException e) {
                Utils.log(TAG, "on  111 " + e.toString() );
            }
    }

    /**
     * Close the stream and socket
     */
    public void cleanUp () {
        stopped = true;
        try {
            if (outputStream != null) {
                outputStream.close();
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
