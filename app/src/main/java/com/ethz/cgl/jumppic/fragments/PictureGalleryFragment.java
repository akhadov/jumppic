package com.ethz.cgl.jumppic.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.ethz.cgl.jumppic.MainActivity;
import com.ethz.cgl.jumppic.PicturePagerAdapter;
import com.ethz.cgl.jumppic.R;

import java.io.File;
import java.util.ArrayList;

public class PictureGalleryFragment extends Fragment {

    private ArrayList<File> pictures;
    private int chosenItem;
    private PicturePagerAdapter mAdapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PictureFragment.
     */
    public static PictureGalleryFragment newInstance(ArrayList<File> pictures, int chosenItem) {
        PictureGalleryFragment fragment = new PictureGalleryFragment();
        fragment.pictures = pictures;
        fragment.chosenItem = chosenItem;
        return fragment;
    }

    public PictureGalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        mAdapter = new PicturePagerAdapter(pictures, getActivity());
        mAdapter.notifyDataSetChanged();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mAdapter.notifyDataSetChanged();
        Button closeBtn;
        Button shareBtn;
        View fragmentPicture = inflater.inflate(R.layout.fragment_picture_gallery, container, false);
        //populate ViewPager with pictures
        ViewPager viewPager = (ViewPager) fragmentPicture.findViewById(R.id.pictures_pager);
        viewPager.setAdapter(mAdapter);
        viewPager.setPageMargin(5);
        viewPager.setCurrentItem(chosenItem);
        closeBtn = (Button) fragmentPicture.findViewById(R.id.pager_close_button);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.button_click);
                //inform service
                a.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        getActivity().getFragmentManager().popBackStack();
                        ((MainActivity)getActivity()).jumpBtn.setVisibility(View.VISIBLE); //fight Nexus 7 bug
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                v.startAnimation(a);
            }
        });
        shareBtn = (Button) fragmentPicture.findViewById(R.id.pager_share_button);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.button_click);
                //inform service
                a.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        startActivity(Intent.createChooser(new Intent().setAction(Intent.ACTION_SEND).setType("image/jpeg").putExtra(Intent.EXTRA_STREAM, Uri.fromFile(pictures.get(chosenItem))), "Share Image"));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                v.startAnimation(a);

                 }
        });
        ((MainActivity)getActivity()).jumpBtn.setVisibility(View.INVISIBLE); //fight Nexus 7 bug
        return fragmentPicture;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
