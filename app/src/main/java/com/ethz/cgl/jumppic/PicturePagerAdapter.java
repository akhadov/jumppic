package com.ethz.cgl.jumppic;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ethz.cgl.jumppic.util.Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sabir on 11.07.2015.
 */
public class PicturePagerAdapter extends PagerAdapter {

    private ArrayList<File> pictures;
    private Activity mActivity;
    private String TAG = PicturePagerAdapter.class.getSimpleName();

    public PicturePagerAdapter(ArrayList<File> pictures, Activity activity) {
        this.pictures = pictures;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //inflate picture view with the appropriate bitmap
        ImageView pictureView;
        TextView pictureTitle;

        LayoutInflater inflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.full_screen_picture_view, container,
                false);
        pictureView = (ImageView) viewLayout.findViewById(R.id.touchImage_picture_view);
        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        pictureView.setImageBitmap(Utils.decodeSampledBitmapFromFile(pictures.get(position), height));
        pictureTitle = (TextView) viewLayout.findViewById(R.id.picture_title);
        pictureTitle.setText(pictures.get(position).getName().substring(0, pictures.get(position).getName().length() - 4));
        container.addView(viewLayout);
        return viewLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout)object);

    }

}
