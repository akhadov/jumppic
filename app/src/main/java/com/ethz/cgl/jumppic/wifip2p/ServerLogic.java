package com.ethz.cgl.jumppic.wifip2p;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.ethz.cgl.jumppic.MainService;
import com.ethz.cgl.jumppic.util.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import static com.ethz.cgl.jumppic.util.Utils.log;
/**
 * http server on Utils.serverPort
 * receives messages from the peer device
 *
 * Created by Sabir on 28.02.2015.
 */
public class ServerLogic {
    private final Handler myHandler;
    private static ServerSocket serverSocket;
    private Socket clientSocket;
    private InputStream inputStream;
    private String TAG = "ServerLogic";
    private boolean stopped;
    private Thread worker;
    ObjectInputStream objectInputStream;

    public ServerLogic(Handler handler){
        this.myHandler = handler;
    }

    /**
     * Wait for the client to connect.
     * Read the data from the inputStream
     * restart the task for continuous reading
     */
    public void startServer () {

        Utils.log(TAG, "method start " + System.currentTimeMillis());

        worker = new Thread(new Runnable() {
            @Override
            public void run() {
                if (stopped)
                    return;
                log("start Server", "Task start " + System.currentTimeMillis());
                try {

                    serverSocket = new ServerSocket(Utils.serverPort);
                    //blocks until the client (re)opens the socket to send new msg
                    try {
                        Utils.log(TAG, "is socket null " + clientSocket);
                        if (clientSocket != null && !clientSocket.isConnected()){
                            clientSocket = null;
                        }
                        if (clientSocket == null || !clientSocket.isConnected() ){
                            Utils.log(TAG, "accepting socket");

                            clientSocket = serverSocket.accept();
                            Utils.log(TAG, "done accepting");
                            Message message = Message.obtain();
                            message.what = MainService.HANDLER_MESSAGE_SET_PEER_IP;
                            Bundle b = new Bundle();
                            b.putString("peer_ip", clientSocket.getInetAddress() + "");
                            message.setData(b);
                            inputStream = clientSocket.getInputStream();
                            objectInputStream = new ObjectInputStream(inputStream);
                            myHandler.sendMessage(message);
                        }
                    } catch (SocketException e) {
                        Utils.log(TAG, e.toString());
                        restartServer();
                        return;
                    }

                    while(!stopped) {
                        log(TAG, "reading message");
                        WiFiMessage message = null;
                        try {
                            if (objectInputStream == null){
                                Utils.log(TAG, "object stream null");
                            }
                            Object objMessage = objectInputStream != null ? objectInputStream.readObject() : null;
                            message = (WiFiMessage) objMessage;
                        }
                        catch (Exception e1) {
                            Utils.log(TAG, e1.getMessage());
                        }
                        if (message == null) {
                            Utils.log(TAG, "read null as message");
                            break;
                        }
                        Message msg = Message.obtain();
                        msg.what = MainService.HANDLER_MESSAGE_NEW_WIFI_MESSAGE;
                        Bundle b = new Bundle();
                        b.putSerializable(MainService.PAYLOAD_NEW_MESSAGE + "", message);
                        msg.setData(b);
                        myHandler.sendMessage(msg);
                        log(TAG, "message send to MainService");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    log(TAG, e.getMessage());
                }
              restartServer();
            }
        });
        worker.start();
    }

    private void restartServer (){
        if (!stopped){try {
            Thread.sleep(5000);
        } catch (InterruptedException e1) {
            Utils.log(TAG, e1.getMessage());
        }
            if (clientSocket != null)
                try {
                clientSocket.close();
                clientSocket = null;
                serverSocket.close();
                serverSocket = null;
                if (inputStream != null)
                    inputStream.close();
                else
                    log("server CleanUp ", "inputStream null");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            startServer();
        }
    }

    /**
     * close all sockets and streams
     */
    public void cleanUp () {
        Utils.log(TAG,"clean up");
        stopped = true;
        try {
            if (worker != null){
                worker.interrupt();
            }
            if (clientSocket != null)
                clientSocket.close();
            clientSocket = null;
            if (serverSocket != null)
                serverSocket.close();
            serverSocket = null;
            if (inputStream != null)
                inputStream.close();
            else
                log("server CleanUp ", "inputStream null");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
